package lifecycle;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class Student implements BeanNameAware, BeanFactoryAware, ApplicationContextAware,InitializingBean, DisposableBean{
	
	/**
	 * 
	 */
	
	private int id;

	private String name;
	
	/**
	 * 
	 */
	
	public Student() {
		System.out.println("["+this.getClass().getSimpleName()+"] ()");
	}
	
	public Student(int id, String name) {
		System.out.println("["+this.getClass().getSimpleName()+"] (int "+id+", String "+name+")");
		this.id = id;
		this.name = name;
	}
	
	/**
	 * 
	 */
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		System.out.println("["+this.getClass().getSimpleName()+"] setId(int "+id+")");
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		System.out.println("["+this.getClass().getSimpleName()+"] setName(String "+name+")");
		this.name = name;
	}

	public String toString(){
		return "["+this.getClass().getSimpleName()+"] toString(id: "+this.id+" name: "+this.name+")";
	}
	
	/**
	 * 
	 */
	
	@PostConstruct
	public void postConstructMethod(){
		System.out.println("["+this.getClass().getSimpleName()+"] postConstructMethod()");
	}
	
	public void setBeanName(String arg0) {
		System.out.println("["+this.getClass().getSimpleName()+"] setBeanName(String "+arg0+")");
	}
	
	public void setBeanFactory(BeanFactory arg0) throws BeansException {
		System.out.println("["+this.getClass().getSimpleName()+"] setBeanFactory(BeanFactory "+arg0+")");
	}
	
	public void setApplicationContext(ApplicationContext arg0) throws BeansException {
		System.out.println("["+this.getClass().getSimpleName()+"] setApplicationContext(ApplicationContext "+arg0+")");
	}
	
	public void initMethod(){
		System.out.println("["+this.getClass().getSimpleName()+"] initMethod()");
	}

	public void afterPropertiesSet() throws Exception {
		System.out.println("["+this.getClass().getSimpleName()+"] afterPropertiesSet()");
	}

	/**
	 * 
	 */
	
	public void destroy() throws Exception {
		System.out.println("["+this.getClass().getSimpleName()+"] destroy()");
	}
	
	public void destroyMethod(){
		System.out.println("["+this.getClass().getSimpleName()+"] destroyMethod()");
	}
	
	@PreDestroy
	public void postPreDestroy(){
		System.out.println("["+this.getClass().getSimpleName()+"] postPreDestroyMethod()");
	}

}
