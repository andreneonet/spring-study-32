package lifecycle;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

//http://www.concretepage.com/spring/example_beanpostprocessor_spring
public class CustomizeBeanProcessor implements BeanPostProcessor {

	public Object postProcessAfterInitialization(Object arg0, String arg1) throws BeansException {
		System.out.println("["+this.getClass().getSimpleName()+"] postProcessAfterInitialization(Object "+arg0+", String "+arg1+")");
		return arg0;
	}

	public Object postProcessBeforeInitialization(Object arg0, String arg1) throws BeansException {
		System.out.println("["+this.getClass().getSimpleName()+"] postProcessBeforeInitialization(Object "+arg0+", String "+arg1+")");
		return arg0;
	}

}
