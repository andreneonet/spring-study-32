package lifecycle;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Run {
	public static void main(String[] args) {
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("beans.xml");
		
		Student student = (Student) applicationContext.getBean("student");
		
		System.out.println(student);
		
		AbstractApplicationContext abstractApplicationContext =  (AbstractApplicationContext) applicationContext;
		
		abstractApplicationContext.registerShutdownHook();
	}
}
